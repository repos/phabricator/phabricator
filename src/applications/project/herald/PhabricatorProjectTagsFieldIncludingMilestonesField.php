<?php

// WMF custom code - see https://phabricator.wikimedia.org/T144041
// Avoid need to update team Herald rules everytime a new sprint is created

final class PhabricatorProjectTagsIncludingMilestonesField
  extends PhabricatorProjectTagsField {

  const FIELDCONST = 'projects.milestones';

  public function getHeraldFieldName() {
    return pht('Project tags incl their milestones (use with None only)');
  }

  public function getHeraldFieldValue($object) {
    // get project PHIDs which currently have an edge with our task $object
    $project_phids = PhabricatorEdgeQuery::loadDestinationPHIDs(
      $object->getPHID(),
      PhabricatorProjectObjectHasProjectEdgeType::EDGECONST);
    // get only those of our current projects which are milestones
    $milestones = id(new PhabricatorProjectQuery())
      ->setViewer(PhabricatorUser::getOmnipotentUser())
      ->withIsMilestone(true)
      ->withPHIDs($project_phids)
      ->execute();
    // pull the parent projects of all our milestones
    $parent_project_phids = mpull($milestones, 'getParentProjectPHID');
    // fake that our task has both milestones and resp parent projects as edges
    return array_merge($project_phids, $parent_project_phids);
  }
}